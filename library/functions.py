import os
import hashlib

from send2trash import send2trash

# Convert selected file list_ to string
# in order to shown them in the window
def list_to_string(list_):
    string = str()
    for i in range(len(list_)):
        string += str(i+1) + " - " + list_[i] + "\n\n"
    return string

# Hash file
def hash_file(file_path):
    # BUF_SIZE is totally arbitrary, change for your app!
    BUF_SIZE = 65536 # lets read stuff in 64kb chunks!

    sha1 = hashlib.sha1()

    with open(file_path, 'rb') as f:
        while True:
            try:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
            except:
                return 1

    return sha1.hexdigest()

def remove_empty_dir(path):
    if os.path.exists(path):
        #os.rmdir(path)
        send2trash(path)

def remove_empty_dirs(path):
    for root, dirnames, filenames in os.walk(path, topdown=False):
        for dirname in dirnames:
            if not os.listdir(os.path.realpath(os.path.join(root, dirname))):
                remove_empty_dir(os.path.realpath(os.path.join(root, dirname)))

# Delete duplicates and empty folders
def delete_duplicate_file(file_):
    if os.path.exists(file_):
        #os.remove(i[j])
        send2trash(file_)

def get_size(paths):
    size = 0
    
    for i in paths:
        size += os.path.getsize(i)

    return size

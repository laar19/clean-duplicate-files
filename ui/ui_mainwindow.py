# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/mainwindow.ui'
#
# Created by: qtpy UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from qtpy import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(880, 614)
        MainWindow.setMinimumSize(QtCore.QSize(800, 600))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setEnabled(True)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label_status_3 = QtWidgets.QLabel(self.frame)
        self.label_status_3.setText("")
        self.label_status_3.setObjectName("label_status_3")
        self.gridLayout_4.addWidget(self.label_status_3, 4, 0, 1, 4)
        self.btn_accept = QtWidgets.QPushButton(self.frame)
        self.btn_accept.setMinimumSize(QtCore.QSize(30, 30))
        self.btn_accept.setIconSize(QtCore.QSize(32, 32))
        self.btn_accept.setObjectName("btn_accept")
        self.gridLayout_4.addWidget(self.btn_accept, 3, 3, 1, 1)
        self.btn_compare_files = QtWidgets.QPushButton(self.frame)
        self.btn_compare_files.setMinimumSize(QtCore.QSize(30, 30))
        self.btn_compare_files.setIconSize(QtCore.QSize(32, 32))
        self.btn_compare_files.setObjectName("btn_compare_files")
        self.gridLayout_4.addWidget(self.btn_compare_files, 1, 3, 1, 1)
        self.label_status = QtWidgets.QLabel(self.frame)
        self.label_status.setObjectName("label_status")
        self.gridLayout_4.addWidget(self.label_status, 3, 0, 1, 1)
        self.btn_search = QtWidgets.QPushButton(self.frame)
        self.btn_search.setMinimumSize(QtCore.QSize(30, 30))
        self.btn_search.setIconSize(QtCore.QSize(32, 32))
        self.btn_search.setObjectName("btn_search")
        self.gridLayout_4.addWidget(self.btn_search, 1, 2, 1, 1)
        self.splitter_2 = QtWidgets.QSplitter(self.frame)
        self.splitter_2.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_2.setObjectName("splitter_2")
        self.layoutWidget_2 = QtWidgets.QWidget(self.splitter_2)
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.label_8 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_8.setObjectName("label_8")
        self.gridLayout_5.addWidget(self.label_8, 0, 0, 1, 1)
        self.white_list = QtWidgets.QTextBrowser(self.layoutWidget_2)
        self.white_list.setDocumentTitle("")
        self.white_list.setObjectName("white_list")
        self.gridLayout_5.addWidget(self.white_list, 1, 0, 1, 5)
        self.label_11 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_11.setObjectName("label_11")
        self.gridLayout_5.addWidget(self.label_11, 0, 2, 1, 1)
        self.white_list_num = QtWidgets.QLabel(self.layoutWidget_2)
        self.white_list_num.setObjectName("white_list_num")
        self.gridLayout_5.addWidget(self.white_list_num, 0, 1, 1, 1)
        self.white_list_size = QtWidgets.QLabel(self.layoutWidget_2)
        self.white_list_size.setObjectName("white_list_size")
        self.gridLayout_5.addWidget(self.white_list_size, 0, 3, 1, 2)
        self.layoutWidget_3 = QtWidgets.QWidget(self.splitter_2)
        self.layoutWidget_3.setObjectName("layoutWidget_3")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.layoutWidget_3)
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label_6 = QtWidgets.QLabel(self.layoutWidget_3)
        self.label_6.setObjectName("label_6")
        self.gridLayout_6.addWidget(self.label_6, 0, 0, 1, 1)
        self.black_list = QtWidgets.QTextBrowser(self.layoutWidget_3)
        self.black_list.setDocumentTitle("")
        self.black_list.setObjectName("black_list")
        self.gridLayout_6.addWidget(self.black_list, 1, 0, 1, 5)
        self.label_10 = QtWidgets.QLabel(self.layoutWidget_3)
        self.label_10.setObjectName("label_10")
        self.gridLayout_6.addWidget(self.label_10, 0, 2, 1, 1)
        self.black_list_num = QtWidgets.QLabel(self.layoutWidget_3)
        self.black_list_num.setObjectName("black_list_num")
        self.gridLayout_6.addWidget(self.black_list_num, 0, 1, 1, 1)
        self.black_list_size = QtWidgets.QLabel(self.layoutWidget_3)
        self.black_list_size.setObjectName("black_list_size")
        self.gridLayout_6.addWidget(self.black_list_size, 0, 3, 1, 2)
        self.gridLayout_4.addWidget(self.splitter_2, 2, 0, 1, 4)
        self.progressBar = QtWidgets.QProgressBar(self.frame)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.gridLayout_4.addWidget(self.progressBar, 5, 0, 1, 4)
        self.splitter = QtWidgets.QSplitter(self.frame)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.layoutWidget = QtWidgets.QWidget(self.splitter)
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_7 = QtWidgets.QLabel(self.layoutWidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout_2.addWidget(self.label_7, 0, 0, 1, 1)
        self.source_paths_list = QtWidgets.QTextBrowser(self.layoutWidget)
        self.source_paths_list.setDocumentTitle("")
        self.source_paths_list.setObjectName("source_paths_list")
        self.gridLayout_2.addWidget(self.source_paths_list, 1, 0, 1, 3)
        self.lcdNumber_4 = QtWidgets.QLCDNumber(self.layoutWidget)
        self.lcdNumber_4.setObjectName("lcdNumber_4")
        self.gridLayout_2.addWidget(self.lcdNumber_4, 0, 1, 1, 2)
        self.layoutWidget1 = QtWidgets.QWidget(self.splitter)
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.layoutWidget1)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_5 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_5.setObjectName("label_5")
        self.gridLayout_3.addWidget(self.label_5, 0, 0, 1, 1)
        self.all_files_list = QtWidgets.QTextBrowser(self.layoutWidget1)
        self.all_files_list.setDocumentTitle("")
        self.all_files_list.setObjectName("all_files_list")
        self.gridLayout_3.addWidget(self.all_files_list, 1, 0, 1, 6)
        self.label_9 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_9.setObjectName("label_9")
        self.gridLayout_3.addWidget(self.label_9, 0, 3, 1, 1)
        self.all_files_num = QtWidgets.QLabel(self.layoutWidget1)
        self.all_files_num.setObjectName("all_files_num")
        self.gridLayout_3.addWidget(self.all_files_num, 0, 1, 1, 2)
        self.all_files_size = QtWidgets.QLabel(self.layoutWidget1)
        self.all_files_size.setObjectName("all_files_size")
        self.gridLayout_3.addWidget(self.all_files_size, 0, 4, 1, 2)
        self.gridLayout_4.addWidget(self.splitter, 0, 0, 1, 4)
        self.label_status_4 = QtWidgets.QLabel(self.frame)
        self.label_status_4.setText("")
        self.label_status_4.setObjectName("label_status_4")
        self.gridLayout_4.addWidget(self.label_status_4, 3, 1, 1, 1)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 6)
        spacerItem = QtWidgets.QSpacerItem(379, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 0, 1, 1)
        self.btn_clear = QtWidgets.QPushButton(self.centralwidget)
        self.btn_clear.setEnabled(True)
        self.btn_clear.setMinimumSize(QtCore.QSize(108, 30))
        self.btn_clear.setIconSize(QtCore.QSize(32, 32))
        self.btn_clear.setObjectName("btn_clear")
        self.gridLayout.addWidget(self.btn_clear, 1, 1, 1, 1)
        self.btn_change_theme = QtWidgets.QPushButton(self.centralwidget)
        self.btn_change_theme.setEnabled(True)
        self.btn_change_theme.setMinimumSize(QtCore.QSize(108, 30))
        self.btn_change_theme.setIconSize(QtCore.QSize(32, 32))
        self.btn_change_theme.setObjectName("btn_change_theme")
        self.gridLayout.addWidget(self.btn_change_theme, 1, 2, 1, 1)
        self.btn_exit = QtWidgets.QPushButton(self.centralwidget)
        self.btn_exit.setMinimumSize(QtCore.QSize(108, 30))
        self.btn_exit.setIconSize(QtCore.QSize(32, 32))
        self.btn_exit.setObjectName("btn_exit")
        self.gridLayout.addWidget(self.btn_exit, 1, 3, 1, 2)
        self.btn_exit.raise_()
        self.btn_change_theme.raise_()
        self.frame.raise_()
        self.btn_clear.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 880, 22))
        self.menuBar.setObjectName("menuBar")
        self.menuHelp = QtWidgets.QMenu(self.menuBar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menuBar)
        self.actionAutores = QtWidgets.QAction(MainWindow)
        self.actionAutores.setObjectName("actionAutores")
        self.actionClasificar = QtWidgets.QAction(MainWindow)
        self.actionClasificar.setObjectName("actionClasificar")
        self.actionBuscar = QtWidgets.QAction(MainWindow)
        self.actionBuscar.setObjectName("actionBuscar")
        self.actionLicencia = QtWidgets.QAction(MainWindow)
        self.actionLicencia.setObjectName("actionLicencia")
        self.actionAcerca_de_Qt = QtWidgets.QAction(MainWindow)
        self.actionAcerca_de_Qt.setObjectName("actionAcerca_de_Qt")
        self.actionAbout_Qt = QtWidgets.QAction(MainWindow)
        self.actionAbout_Qt.setObjectName("actionAbout_Qt")
        self.actionAuthors = QtWidgets.QAction(MainWindow)
        self.actionAuthors.setObjectName("actionAuthors")
        self.actionLicense = QtWidgets.QAction(MainWindow)
        self.actionLicense.setObjectName("actionLicense")
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionthird_party = QtWidgets.QAction(MainWindow)
        self.actionthird_party.setObjectName("actionthird_party")
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionAbout_Qt)
        self.menuHelp.addAction(self.actionAuthors)
        self.menuHelp.addAction(self.actionLicense)
        self.menuHelp.addAction(self.actionthird_party)
        self.menuBar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Clean duplicate files and empty directories"))
        self.btn_accept.setText(_translate("MainWindow", "Clean duplicated files"))
        self.btn_compare_files.setText(_translate("MainWindow", "Compare files"))
        self.label_status.setText(_translate("MainWindow", "Ready            "))
        self.btn_search.setText(_translate("MainWindow", "Search files"))
        self.label_8.setText(_translate("MainWindow", "Unique files:"))
        self.label_11.setText(_translate("MainWindow", "Size"))
        self.white_list_num.setText(_translate("MainWindow", "0"))
        self.white_list_size.setText(_translate("MainWindow", "0 Kb"))
        self.label_6.setText(_translate("MainWindow", "Duplicated files:"))
        self.label_10.setText(_translate("MainWindow", "Size"))
        self.black_list_num.setText(_translate("MainWindow", "0"))
        self.black_list_size.setText(_translate("MainWindow", "0 Kb"))
        self.label_7.setText(_translate("MainWindow", "Source paths"))
        self.label_5.setText(_translate("MainWindow", "All files:"))
        self.label_9.setText(_translate("MainWindow", "Size"))
        self.all_files_num.setText(_translate("MainWindow", "0"))
        self.all_files_size.setText(_translate("MainWindow", "0 Kb"))
        self.btn_clear.setText(_translate("MainWindow", "Clear all"))
        self.btn_change_theme.setText(_translate("MainWindow", "Change Theme"))
        self.btn_exit.setText(_translate("MainWindow", "Quit"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.actionAutores.setText(_translate("MainWindow", "Autores"))
        self.actionClasificar.setText(_translate("MainWindow", "Clasificar"))
        self.actionBuscar.setText(_translate("MainWindow", "Buscar"))
        self.actionLicencia.setText(_translate("MainWindow", "Licencia"))
        self.actionAcerca_de_Qt.setText(_translate("MainWindow", "Acerca de Qt"))
        self.actionAbout_Qt.setText(_translate("MainWindow", "About Qt"))
        self.actionAuthors.setText(_translate("MainWindow", "Authors"))
        self.actionLicense.setText(_translate("MainWindow", "License"))
        self.actionAbout.setText(_translate("MainWindow", "About"))
        self.actionthird_party.setText(_translate("MainWindow", "Third party"))

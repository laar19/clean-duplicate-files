# coding: utf-8

import os
import sys
import qdarkstyle

import pandas as pd

from qtpy.QtWidgets import QApplication, QMainWindow
from qtpy.QtGui     import QPixmap, QIcon
from qtpy           import QtWidgets

from qdarkstyle.dark.palette  import DarkPalette
from qdarkstyle.light.palette import LightPalette

from humanize import naturalsize

from library.functions import *
from ui.ui_mainwindow  import Ui_MainWindow

appname  = "CleanDuplicateFiles"

about    = appname + " version 1.0\n\nThis program clean duplicate files \
\nand empty directories"
    
authors  = ["Luis Acevedo", "<laar@pm.me>"]

license_ = "Copyright 2020. All code is copyrighted by the respective authors.\n" \
+ appname + " can be redistributed and/or modified under the terms of \
the GNU GPL versions 3 or by any future license endorsed by " + authors[0] + \
".\nThis program is distributed in the hope that it will be useful, but \
WITHOUT ANY WARRANTY; without even the implied warranty of \
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
    
third_party = "App logo - Icons by Orion Icon Library - https://orioniconlibrary.com"

# Resources path
tmp_path1 = "ui/resources/img/"
tmp_path2 = "CleanDuplicateFiles.AppDir/usr/bin/ui/resources/img/"
path      = str()

if(os.path.exists(tmp_path1)):
    path = tmp_path1
else:
    path = tmp_path2

paths       = list() # Files directories source
black_list  = list() # Files to be deleted
error_files = list() # Files failed

class MainWindow(QMainWindow, Ui_MainWindow):
    """
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.connectSignalsSlots()

    def connectSignalsSlots(self):
        self.progressBar.setValue(0) # Progress bar
        
        # Search files button
        pixmap = QIcon(path+"Start-Menu-Search-icon.png")
        self.btn_search.setIcon(pixmap)
        self.btn_search.clicked.connect(self.search)
        
        # Compare files button
        self.btn_compare_files.clicked.connect(self.compare_files)

        # Depurate button
        pixmap = QIcon(path+"Accept-icon.png")
        self.btn_accept.setIcon(pixmap)
        self.btn_accept.clicked.connect(self.depurate)

        # Clear file list button
        pixmap = QIcon(path+"Actions-edit-clear-locationbar-rtl-icon.png")
        self.btn_clear.setIcon(pixmap)
        self.btn_clear.clicked.connect(self.clear)

        # About
        self.actionAbout.triggered.connect(self.about_)
        
        # About Qt
        self.actionAbout_Qt.triggered.connect(self.aboutQt)
        
        # Authors
        self.actionAuthors.triggered.connect(self.authors_)
        
        # License
        self.actionLicense.triggered.connect(self.license_)

        # Third party
        self.actionthird_party.triggered.connect(self.third_party_)

        # Change theme
        self.btn_change_theme.setStyleSheet(
            "QPushButton { background-color: purple; } \
                QPushButton::hover { \
                background-color: grey; \
            }"
        )
        self.btn_change_theme.setCheckable(True)
        #self.btn_change_theme.setChecked(True)
        self.btn_change_theme.clicked.connect(self.toggle_theme)

        # Exit
        self.btn_exit.clicked.connect(self.exit)

    # Search files in file system
    def search(self):
        global paths

        tmp = QtWidgets.QFileDialog.getSaveFileName(self, ("Select Folder"), "SELECT SOURCE FOLDER",)
        tmp = tmp[0].split("/")

        if len(tmp) > 1:
            source_path = ""
            for i in tmp[1:-1]:
                source_path += i + "/"
            source_path = "/" + source_path

            # Avoid duplicated paths
            if source_path not in paths:
                paths += [source_path]

        self.lcdNumber_4.setProperty("value", len(paths))
        self.source_paths_list.setText(list_to_string(paths))

    def compare_files(self):
        global paths

        # Get all files paths
        tmp            = list()
        all_files_size = 0
        if len(paths) > 0:
            self.frame.setEnabled(False)
            self.label_status.setText("Getting file list")
            
            for i in paths:
                for path, subdirs, files in os.walk(i):
                    #print(dirpath)
                    for name in files:
                        #if ".txt" in file:
                        file_path = os.path.join(path, name)
                        self.label_status_3.setText(file_path)
                        tmp.append(file_path)
                        all_files_size += os.path.getsize(file_path)

            self.all_files_num.setText(str(len(tmp)))
            self.all_files_size.setText(naturalsize(all_files_size))
            self.all_files_list.setText(list_to_string(tmp))

            self.label_status.setText("Getting file list done")

            # Get hashes from paths
            completed = 0
            # List of files to be cleaned
            hashed_file_list = {"hash": list(), "path": list()}
            self.progressBar.setValue(1)
            self.label_status.setText("Hashing files")
            for i in range(len(tmp)):
                self.label_status_3.setText(str(tmp[i]))
                self.label_status_4.setText(str(i+1)+" of "+str(len(tmp)))
                file_hash = hash_file(tmp[i])
                if file_hash == 1:
                    global error_files
                    error_files.append(tmp[i])
                else:
                    hashed_file_list["hash"].append(file_hash)
                    hashed_file_list["path"].append(tmp[i])
                    completed = self.update_progress_bar(len(tmp), completed)
            
            self.label_status.setText("Hashing files done")
            self.progressBar.setValue(100)

            df_hashed_file_list = pd.DataFrame(hashed_file_list)
            hashed_file_list    = {"hash": list(), "path": list()}

            white_list = df_hashed_file_list.drop_duplicates(subset=["hash"])
            
            global black_list
            black_list = df_hashed_file_list.drop(list(white_list.index))

            size = get_size(white_list["path"])
            self.white_list_num.setText(str(len(white_list)))
            self.white_list_size.setText(naturalsize(size))
            self.white_list.setText(list_to_string(list(white_list["path"])))

            size = get_size(black_list["path"])
            self.black_list_num.setText(str(len(black_list)))
            self.black_list_size.setText(naturalsize(size))
            self.black_list.setText(list_to_string(list(black_list["path"])))

            self.frame.setEnabled(True)
        else:
            QtWidgets.QMessageBox.information(self, "Warning", "You must have selected at least one folder")

    # Clean duplicated files
    def depurate(self):
        global black_list
        if len(black_list) > 0:
            aux = list()
            
            completed = 0

            self.frame.setEnabled(False)

            self.label_status.setText("Deleting duplicates")

            main_function = delete_duplicate_file
            
            aux = list(black_list["path"])

            if len(aux) == 0:
                QtWidgets.QMessageBox.information(self, "Warning", "There is no duplicated files")
            else:
                button_reply = QtWidgets.QMessageBox.question(self, "Confirm", "Proceed")
                if button_reply == QtWidgets.QMessageBox.Yes:
                    self.label_status.setText("Cleaning...")
                    self.progressBar.setValue(1)
                    for i in range(len(aux)):
                        try:
                            main_function(aux[i])
                            completed = self.update_progress_bar(len(aux), completed)
                        except:
                            QtWidgets.QMessageBox.critical(self, "Error", "An Error happened duing the cleaning.\n" + "The file: " + aux[i] + "\nCould be corrupted or damaged")

                    # Remove empty dirs
                    for i in paths:
                        remove_empty_dirs(i)
                        
                    self.label_status.setText("Deleting done")
                    self.progressBar.setValue(100)
                    QtWidgets.QMessageBox.about(self, "Done", "Cleaning success")
                    if len(error_files) > 0:
                        QtWidgets.QMessageBox.about(self, "Files failed", list_to_string(error_files))

            self.frame.setEnabled(True)
        else:
            QtWidgets.QMessageBox.information(self, "Warning", "There is no duplicated files")

    # Clear selected file list
    def clear(self):
        self.label_status.setText("Ready            ")
        self.label_status_3.setText("")
        self.label_status_4.setText("")
        
        self.source_paths_list.setText("")
        self.all_files_list.setText("")
        self.white_list.setText("")
        self.black_list.setText("")
        
        self.all_files_num.setText("0")
        self.white_list_num.setText("0")
        self.black_list_num.setText("0")
        
        self.all_files_size.setText("0 Kb")
        self.white_list_size.setText("0 Kb")
        self.black_list_size.setText("0 Kb")
        
        paths.clear()
        
        black_list  = list()
        error_files = list()
        
        self.progressBar.setValue(0)
        self.lcdNumber_4.setProperty("value", 0)

    def update_progress_bar(self, max_, completed):
        increment = 100 / max_

        if completed < 100:
            completed += increment
            self.progressBar.setValue(int(completed))
        
        return completed
    
    def about_(self):
        QtWidgets.QMessageBox.about(self, "About", about)

    def aboutQt(self):
        QtWidgets.QMessageBox.aboutQt(self)
        
    def authors_(self):
        text = authors[0] + " " + authors[1] + "\n"
        QtWidgets.QMessageBox.about(self, "Authors", text)
        
    def license_(self):
        QtWidgets.QMessageBox.about(self, "License", license_)

    def third_party_(self):
        QtWidgets.QMessageBox.about(self, "Third party", third_party)

    def toggle_theme(self):
        if not self.btn_change_theme.isChecked():
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))
        else:
            app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=LightPalette))

    def exit(self):
        sys.exit()

if __name__ == "__main__":
    
    print("\n" + appname + " Copyright (C) 2022 " + authors[0] + ".\nEste programa viene con ABSOLUTAMENTE NINGUNA GARANTÍA.\nEsto es software libre, y le invitamos a redistribuirlo\nbajo ciertas condiciones.\nPor favor, leer el archivo README.")

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api="pyside2", palette=DarkPalette))

    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
